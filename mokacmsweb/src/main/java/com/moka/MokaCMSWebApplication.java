package com.moka;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.ApplicationContext;


import java.util.Arrays;

//@EnableWebMvc
@SpringBootApplication
@MapperScan(basePackages = {"com.moka.mapper"})
public class MokaCMSWebApplication {
//	 extends SpringBootServletInitializer
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(MokaCMSWebApplication.class);
//	}
	public static void main(String[] args) {
		ApplicationContext applicationContext= SpringApplication.run(MokaCMSWebApplication.class,args);
//		String[] names = applicationContext.getBeanDefinitionNames();
//		Arrays.asList(names).forEach(name -> System.out.println(name));//1.8 forEach循环
	}
}
