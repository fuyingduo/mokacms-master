package com.moka.util;

import java.sql.*;

public class JdbcUtil {

  public static Connection getConnection() {
    Connection con = null;

    String driverName = "com.mysql.jdbc.Driver";
//    String dbURL = "jdbc:mysql://localhost:3306/moka?useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&failOverReadOnly=false";
    String dbURL="jdbc:mysql://rm-bp1l5u6r7dpt8c3pc.mysql.rds.aliyuncs.com:3306/wymk?useUnicode=true&characterEncoding=UTF-8";
    String userName = "wymk_server";
    String userPwd = "wuyouMoka888";
    try {
      Class.forName(driverName);
      con =  DriverManager.getConnection(dbURL, userName, userPwd);
    } catch (Exception e) {
      System.out.println("获取连接失败." + e.getMessage());
    }
    return con;
  }

  public static void main(String[] args) throws SQLException {
    Connection con= getConnection();
    Statement statement = con.createStatement();
    String sql = "select * from sys_user";
   ResultSet rs = statement.executeQuery(sql);
   while(rs.next()){
     System.out.println(rs.getString("username"));
   }

  }
}