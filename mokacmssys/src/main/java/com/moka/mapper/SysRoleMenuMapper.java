package com.moka.mapper;


import com.moka.base.BaseMapper;
import com.moka.entity.SysRoleMenu;

import java.util.List;

public interface SysRoleMenuMapper  extends BaseMapper<SysRoleMenu,String> {

    int deleteByPrimaryKey(SysRoleMenu key);

    int insert(SysRoleMenu record);

    int insertSelective(SysRoleMenu record);

    List<SysRoleMenu> selectByCondition(SysRoleMenu sysRoleMenu);

   int  selectCountByCondition(SysRoleMenu sysRoleMenu);
}