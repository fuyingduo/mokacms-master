package com.moka.service;


import com.moka.base.BaseService;
import com.moka.entity.SysRoleUser;

import java.util.List;

/**
 * @author langmingsheng
 */
public interface RoleUserService  extends BaseService<SysRoleUser,String> {

  int deleteByPrimaryKey(SysRoleUser sysRoleUser);

  int insert(SysRoleUser sysRoleUser);

  int selectCountByCondition(SysRoleUser sysRoleUser);

  List<SysRoleUser> selectByCondition(SysRoleUser sysRoleUser);
}
