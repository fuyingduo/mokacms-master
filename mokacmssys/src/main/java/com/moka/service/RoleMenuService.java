package com.moka.service;


import com.moka.base.BaseService;
import com.moka.entity.SysRoleMenu;

import java.util.List;

/**
 * @author langmingsheng
 */
public interface RoleMenuService extends BaseService<SysRoleMenu,String> {

    List<SysRoleMenu> selectByCondition(SysRoleMenu sysRoleMenu);

    int  selectCountByCondition(SysRoleMenu sysRoleMenu);

    int deleteByPrimaryKey(SysRoleMenu sysRoleMenu);
}
