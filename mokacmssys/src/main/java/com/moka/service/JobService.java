package com.moka.service;


import com.moka.base.BaseService;
import com.moka.entity.SysJob;

/**
 * @author langmingsheng
 */
public interface JobService extends BaseService<SysJob,String> {

}
