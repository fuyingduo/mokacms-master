package com.moka.service.impl;


import com.moka.base.BaseMapper;
import com.moka.base.impl.BaseServiceImpl;
import com.moka.entity.SysJob;
import com.moka.mapper.SysJobMapper;
import com.moka.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author langmingsheng
 */
@Service
public class JobServiceImpl  extends BaseServiceImpl<SysJob,String> implements JobService {

  @Autowired
  SysJobMapper jobMapper;
  @Override
  public BaseMapper<SysJob, String> getMappser() {
    return jobMapper;
  }
}
